package org.itstep.qa.lesson.sql;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;

public class Lunapark {


    public static void main(String[] args) {
        Connection connection = null;


        Statement statement = null;
        String query = "select * from type_a";

        String queryUpdateTable = "insert into type_a(type_attraction) " +
                " values ( 'stationary') , ('mobile')";

        try {
            connection = DriverManager.getConnection

                    ("jdbc:mysql://localhost:3306/parkAtt", "root", "кщще");

            statement = connection.createStatement();
            //выполнение запроса на вставку
            statement.executeUpdate(queryUpdateTable);
            //выполнение запроса на вывборку данных
            ResultSet rc = statement.executeQuery(query);
            //обработка полученного множества строк из базы данных
            while (rc.next()) {
                System.out.println(rc.getString("type_attraction"));
            }

            query = "select * from attraction";

            queryUpdateTable = "insert into attraction(name_attraction, seats, type_att) " +
                    " values ('Kolobok',34,2), ('Lodochka',6,1), ('Aist',5,1),('Koleso',120,2) ";
            statement = connection.createStatement();

            statement.executeUpdate(queryUpdateTable);
            rc = statement.executeQuery(query);

            while (rc.next()) {
                System.out.println(rc.getString("name_attraction")
                        + "|" +
                        rc.getString("seats")
                        + "|" +
                        rc.getString("type_att"));
            }
            //Написать код, который выводит следующую информацию:
            //Количество аттракционов

            query = "select count(*)name_attraction from attraction";

            statement = connection.createStatement();
            rc = statement.executeQuery(query);

                while (rc.next()) {
                    int count = rc.getInt(1);
                    System.out.println("Количество аттракционов : " + count);
                }

           // Только передвижные аттракционы

                query = "select name_attraction from attraction,type_a where attraction.type_att=type_a.id and attraction.type_att='2'";

            statement = connection.createStatement();
            rc = statement.executeQuery(query);
            while (rc.next()) {

                String mobileAtt = rc.getString("name_attraction");

                System.out.println("Передвижные аттракционы : " + mobileAtt);
        }
            //Аттракционы, количество посадочных мест у которых больше 33

            query = "select name_attraction from attraction,type_a where attraction.type_att=type_a.id and attraction.seats >'33'";

            statement = connection.createStatement();
            rc = statement.executeQuery(query);
            while (rc.next()) {

                String bigAtt = rc.getString("name_attraction");

                System.out.println("Аттр с количеством мест больше 31 : " + bigAtt);
            }
            //Суммарное количество мест у передвижных и стационарных аттракционов

            query = "SELECT SUM(seats) FROM attraction";
            statement = connection.createStatement();
            rc = statement.executeQuery(query);

            while (rc.next()) {
                int count = rc.getInt(1);
                System.out.println("Количество мест : " + count);
            }

                rc.close();
                statement.close();
                connection.close();

            } catch (SQLException ex) {

                System.out.println("При работе с БД произошла ошибка!");
                ex.printStackTrace();
            }
        }
    }








